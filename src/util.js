var STATUS = Object.freeze({
    EMPTY: 1,
    CIRCLE: 2,
    CROSS: 3
});

var TURN = Object.freeze({
    INACTIVE: 1,
    PLAYER1: 2,
    PLAYER2: 3
});

export { TURN, STATUS };
