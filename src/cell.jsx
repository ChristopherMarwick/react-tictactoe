import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {STATUS} from './util'


class Cell extends Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        status: STATUS.EMPTY
    }

    getColor = () => {
        switch (this.props.status) {
            case STATUS.EMPTY:
                return 'white';
            case STATUS.CIRCLE:
                return 'blue';
            case STATUS.CROSS:
                return 'red';
        }
    }

    handleClick(e) {
        this.props.callback(e, this.props.x, this.props.y);
    }

    render() {
        return <div onClick={this.handleClick.bind(this)} style={{
            background: this.getColor(),
            'borderStyle': 'solid',
            'borderColor': 'black',
            width: '50px',
            height: '50px'
        }}></div>
    }
}

export default Cell;
