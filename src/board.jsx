import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cell from './cell';
import { STATUS, TURN } from './util';


class Board extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cells: this.getEmptyCells(),
            turn : TURN.PLAYER1,
            wonStatus: ''
        };
    }

    getEmptyCells = () => {
        var cells = [];
        for (var y=0; y<4; y++) {
            cells.push([STATUS.EMPTY, STATUS.EMPTY, STATUS.EMPTY]);
        };
        return cells;
    }

    restart = () => {
        // Two questions:
        // - How to refresh child cells if we're not allowed to mutate props?
        // Make the prop passed to the child a state from myself, then simply need to redraw!!!
        // But then if we redraw we're mandating doing this...
        // - How to pass props into a html element like <Cell/>?
        this.setState({
            cells: this.getEmptyCells(),
            turn: TURN.PLAYER1,
            wonStatus: ''
        });
    }

    isWon = () => {
        let fail = false;

        // Check rows
        for (var y=0; y<3; y++) {
            if (this.state.cells[y].every((val) => val == STATUS.CIRCLE) ||
                    this.state.cells[y].every((val) => val == STATUS.CROSS)) {
                return true;
            }
        }

        // Check columns
        for (var x=0; x<3; x++) {
            var firstVal = this.state.cells[0][x];
            fail = false;
            if ([STATUS.CIRCLE, STATUS.CROSS].indexOf(firstVal) === -1) {
                continue;
            }
            for (var y=0; y<3; y++) {
                console.log('Check cell y={y}, x={x}'.replace('{y}', y).replace('{x}', x));
                if (this.state.cells[y][x] != firstVal) {
                    console.log('Breaking');
                    fail = true;
                    break;
                }
            }
            if (!fail) {
                return true;
            }
        }

        // Check diagonals
        firstVal = this.state.cells[0][0];
        fail = false;
        if ([STATUS.CIRCLE, STATUS.CROSS].indexOf(firstVal) !== -1) {
            for (var i=0; i<3; i++) {
                if (this.state.cells[i][i] != firstVal) {
                    fail = true;
                    break;
                }
            }
            if (!fail) {
                return true;
            }
        }

        firstVal = this.state.cells[2][0];
        fail = false;
        if ([STATUS.CIRCLE, STATUS.CROSS].indexOf(firstVal) !== -1) {
            for (var i=0; i<3; i++) {
                if (this.state.cells[2-i][i] != firstVal) {
                    fail = true;
                    break;
                }
            }
            if (!fail) {
                return true;
            }
        }
    }

    setCellState = (e, x, y) => {
        // If the game is ended or the cell is already set then disallow players
        // setting the cell.
        if (this.state.turn === TURN.INACTIVE || this.state.cells[y][x] !== STATUS.EMPTY) {
            return;
        }

        var newCells = this.state.cells.slice();
        var nextTurn;
        var wonStatus = '';
        newCells[y][x] = this.state.turn == TURN.PLAYER1 ? STATUS.CIRCLE : STATUS.CROSS;
        if (this.isWon()) {
            // Player in this.state.turn has won
            nextTurn = TURN.INACTIVE;
            wonStatus = '{player} has won!'.replace('{player}', this.state.turn == TURN.PLAYER1 ? 'Player 1' : 'Player 2');
        } else {
            nextTurn = this.state.turn == TURN.PLAYER1 ? TURN.PLAYER2 : TURN.PLAYER1;
        }
        this.setState({
            cells: newCells,
            turn: nextTurn,
            wonStatus: wonStatus
        });
    }

    resetBoard = (e) => {
        this.restart();
    }

    createRow = (y) => {
        var row = [];
        var key;
        for (var x=0; x<3; x++) {
            key = '%x,%y'.replace('%x', x).replace('%y', y);
            row.push(<td key={key}><Cell
                status={this.state.cells[y][x]}
                callback={this.setCellState.bind(this)}
                x={x}
                y={y}
            /></td>);
        }
        return <tr key={y.toString()}>
            {row}
        </tr>
    }

    createTable = () => {
        var table = [];
        for (var y=0; y<3; y++) {
            table.push(this.createRow(y));
        }
        return <table>
            <tbody>
            {table}
            </tbody>
        </table>
    }
    // createTable() {
    //     let rows = [<table>];
    //     for (let i=0; i<3; i++) {
    //         rows.push(<tr>abc</tr>)
    //     }
    //     rows.push(</table>)
    //     return {rows}
    // }

    render = () => {
        return <div>
            {this.createTable()}
            <br/>
            <label>{this.state.wonStatus}</label>
            <br/>
            <input type="button" value="Reset" onClick={this.resetBoard.bind(this)}/>
        </div>
    }
}

export default Board;
