import React from 'react';
import ReactDOM from 'react-dom';
import Board from './board'
import $ from 'jquery';

ReactDOM.render(
    <Board/>, document.getElementById('root')
);